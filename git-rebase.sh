#!/bin/bash


if [ -z "$1" ]; then
    sourcePath=.
else
    sourcePath=$1
fi

echo `pwd` $sourcePath

for dir in `ls -d $sourcePath/*`; do
    if [ -d $dir/.git ]; then
        echo Rebase `basename $dir`
        make `basename $dir`-rebase
    fi
done
