#!/bin/bash


if [ -z "$1" ]; then
    sourcePath=.
else
    sourcePath=$1
fi

echo "Git status for proyects in" $sourcePath

if [ -d $sourcePath ]; then
    cd $sourcePath
else
    echo "Directory " $sourcePath "does not exist"
    exit
fi


for dir in `ls -d *`; do
    if [ -d $dir/.git ]; then
        cd $dir
        status=`git status --porcelain`
        rev=`git rev-parse --short HEAD`
        branch=`git branch | grep ^\* | cut -d "*" -f2`
        if [ -z "$status" ]; then
            echo -e " \033[32m--" $dir "["$branch" ]" "("$rev")" "\e[0m"
        else
            echo -e " \033[31m--" $dir "["$branch" ]" "("$rev")" "\e[0m"
            git status --porcelain
        fi

        cd ..
    fi
done
